/* Class to help reading and writing json data to nfc tags */
(function(){

    var type = {

        // Gets array of objects from the NFC tag. Returns empty array if tag is empty.
        // tagData is the ndef.tag object
        readItems : function (tagData){
            var result = [];

            if (tagData.ndefMessage && tagData.ndefMessage.length > 0){
                for(var i = 0 ; i < tagData.ndefMessage.length ; i ++){
                    var item = tagData.ndefMessage[i];

                    var json = item.payload ? nfc.bytesToString(item.payload) : "";
                    var type = item.type ? nfc.bytesToString(item.type) : "";

                    if (!json || !type || !type === "json/payload"){
                        continue;
                    }

                    var obj = jQuery.parseJSON(json);
                    result.push(obj);
                }
            }

            return result;
        },

        // writes the given items to the NFC tag.
        writeItems : function( items, onSuccess, onError ){
            var records = [];

            for (var i = 0 ; i < items.length ; i ++){

                var item = items[i];
                var bytes = nfc.stringToBytes(JSON.stringify(item));
                var record = ndef.mimeMediaRecord("json/payload", bytes);

                records.push(record);
            }

            nfc.write( records,  onSuccess,  onError );
        }

    };

    TagRunner.TagHelper = type;

}())
