(function(){

    klon.register("haku.views.baton", haku.views.base.basic.type().extend({

        events : {
            "click #btnHome" : "onHome"
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/baton' }).text);
        },

        onShow : function (){
            if (haku.app.context.baton){
                var self = this;
                $(haku.app).on("batonTick", function(){
                    self.onTick();
                });
            }
        },

        onTick : function(){
            this.$el.find('#inner-content').show();

            this.$el.find("#healthDisplay").html(haku.app.context.baton.hp);
            this.$el.find("#timeDisplay").html(haku.app.context.baton.time);
            this.$el.find("#damage").html(haku.app.context.baton.time * haku.app.context.baton.hp);

       },

        onHome : function(){
            haku.app.context.reset();
        }

    }));

}());