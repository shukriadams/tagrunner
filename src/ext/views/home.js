(function(){

    klon.register("haku.views.home", haku.views.base.basic.type().extend({

        events : {
            "change #txtName" : "nameChanged"
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/home' }).text);
        },

        onShow : function (){
            var name = localStorage.getItem("playername");
            if (name){
                this.$el.find('#txtName').val(name);
            }
        },

        getName : function(){
            return this.$el.find('#txtName').val();
        },

        nameChanged : function(){
            localStorage.setItem("playername", this.$el.find('#txtName').val());
        }

    }));

}());