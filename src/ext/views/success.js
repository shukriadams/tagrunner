(function(){

    klon.register("haku.views.success", haku.views.base.basic.type().extend({

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/success' }).text);
        },

        onShow : function (){
            if (haku.app.context.score != undefined){
                this.$el.find('#displayScore').html(haku.app.context.score);
            }

            if (haku.app.context.newScore != null){
                this.$el.find('#phUpdateScore').show();
            } else {
                this.$el.find('#phNoHighScore').show();
            }
        }

    }));

}());