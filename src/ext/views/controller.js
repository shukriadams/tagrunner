(function(){

    klon.register("haku.views.controller", Backbone.View.extend({

        events : {
            "click #btnTriggerStart" : "onStartTag",
            "click #btnTriggerEnd" : "onEndTag",
            "click #btnAdmin" : "onAdmin",
            "click #btnBack" : "onBack"
        },

        initialize : function(){
            var self = this;

            nfc.shim.onWrite = function() {
                self.onTagWrite();
            };

        },

        render : function(){
            var template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/controller' }).text);
            this.$el.html(template());

            this.$el.find("[data-role='accel-pad']").mouseout(function(e){
                $('[data-role="accel-x"]').html(0);
                $('[data-role="accel-y"]').html(0);

                navigator.accelerometer.shim.x = 0;
                navigator.accelerometer.shim.y = 0;
            });

            this.$el.find("[data-role='accel-pad']").mousemove(function(e){
                if(e.which !== 1)
                    return;

                var parentOffset = $(this).parent().offset();
                var relX = e.pageX - parentOffset.left;
                var relY = e.pageY - parentOffset.top;

                $('[data-role="accel-x"]').html(relX);
                $('[data-role="accel-y"]').html(relY);

                navigator.accelerometer.shim.x = relX;
                navigator.accelerometer.shim.y = relX;

            });

            var self = this;
            $(haku.app).on('startTagWrite', function(){
                self.onStartTagWrite();
            });
            $(haku.app).on('endTagWrite', function(){
                self.onEndTagWrite();
            });
        },

        // Invoked when start tag is written to from elsewhere in system.
        // Serializes tag data.
        onStartTagWrite : function(){
            var json = JSON.stringify(nfc.shim.data);
            localStorage.setItem("startTag", json);
        },

        // Invoked when end tag is written to from elsewhere in system.
        // Serializes tag data.
        onEndTagWrite : function(){
            var json = JSON.stringify(nfc.shim.data);
            localStorage.setItem("endTag", json);
        },

        onStartTag : function(){
            var raw = localStorage.getItem("startTag");
            if (raw){
                nfc.shim.data = JSON.parse(raw);
            }

            nfc.shim.raiseDetect();
        },

        onEndTag : function(){
            var raw = localStorage.getItem("endTag");
            if (raw){
                nfc.shim.data = JSON.parse(raw);
            }

            nfc.shim.raiseDetect();
        },

        onTagWrite : function(){
            console.log(JSON.stringify(nfc.shim.data));
        },

        onAdmin : function(){
            document.dispatchEvent(new CustomEvent("menubutton"));
        },

        onBack : function(){
            document.dispatchEvent(new CustomEvent("backbutton"));
        }

    }));

}());