(function(){

    klon.register("haku.views.highScore", haku.views.base.basic.type().extend({

        events : {
            "click #btnHome" : "onHome"
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/highScore' }).text);
        },

        onShow : function (){
            if (haku.app.context.displayHighScoreList && haku.app.context.displayHighScoreList.length > 0){
                var host = this.$el.find('[data-role="high-score-holder"]');

                var template = _.template("<tr><td> {{i}} </td><td> {{ name }} </td>:<td> {{ score }} </td>");

                for (var i = 0 ; i < haku.app.context.displayHighScoreList.length ; i ++){
                    var score = haku.app.context.displayHighScoreList[i];
                    var html = template({ i : i + 1 , name: score.name, score : score.score });
                    host.append(html);
                }
            }
        },

        onHome : function(){
            haku.app.context.reset();
        }

    }));

}());