(function(){

    klon.register("haku.views.admin", haku.views.base.basic.type().extend({

        events : {
            "click #btnHome" : "onHome"
        },

        loadTemplate : function(){
            this.template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/admin' }).text);
        },

        onShow : function (){
            var self = this;
            document.addEventListener("backbutton", function(){
                self.onHome();
            }, false);
        },

        getValue : function(){
            return this.$el.find('[name="rbAction"]:checked').val();
        },

        onHome : function(){
            $(haku.app).trigger('gameContext')
        }

    }));

}());