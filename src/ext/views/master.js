(function(){

    klon.register("haku.views.master",Backbone.View.extend({

        render : function(){
            var template = _.template(new EJS({ url: haku.settings.systemPathRoot + 'ext/templates/master' }).text);
            this.$el.html(template());
        }

    }));

}());