// hp
// time
(function(){

    var type = function(){

        this.hp = 1;
        this.time = 1;

        var self = this;
        self.timerId = setInterval(function(){
            self.time ++ ;

            $(haku.app).trigger('batonTick');

        }, 1000);
    };

    type.prototype = function () { this.apply(this, arguments); };

    type.prototype.dispose = function () {
        if (this.timerId != null){
            clearInterval(this.timerId);
        }
    };

    klon.register("TagRunner", "Baton", type);

}())