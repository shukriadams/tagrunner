(function(){

    // clear tag
    // reset highschore
    // make start
    // make end
    // diagnose
    var type = function(){
        var self = this;
        $(haku.app).on("tag", function(sender, nfcEvent){
            if (haku.app.context != self)
                return;

            self.onTag(sender, nfcEvent);
        });
    };

    type.prototype = function () { this.apply(this, arguments); };

    type.prototype.onTag = function(sender, nfcEvent){

        this.clear();
        this.log("Tag detected. ");
        var self = this;

        if (haku.router.currentView.getValue){

            var val = haku.router.currentView.getValue();

            if (val === "diagnose"){
                var raw = JSON.stringify(nfcEvent.tag);
                self.log(raw);
            } else if (val === "blank"){
                nfc.erase(function(){
                    self.log("Tag erased");

                    $(haku.app).trigger("tagErased");

                }, function(err){
                    self.log("Tag erase error : " + err);
                });
            } else if (val === "makeEnd"){

                var t = { type : "endTag"};

                TagRunner.TagHelper.writeItems(
                    [t],
                    function(){
                        self.log("End tag created");

                        $(haku.app).trigger("endTagWrite");
                    },
                    function(err){
                        self.log("Error creating end tag : " + err);
                    }
                )

            } else if (val === "makeStart"){

                // get all items on tag
                var t = { type : "startTag"};

                // try to get sensitivity
                if (haku.router.currentView.$el.find('#txtSensitivity').val().length > 0) {
                    t.sensitivity = parseInt(haku.router.currentView.$el.find('#txtSensitivity').val());
                }

                TagRunner.TagHelper.writeItems(
                    [t],
                    function(){
                        self.log("Start tag created");

                        $(haku.app).trigger("startTagWrite");
                    },
                    function(err){
                        self.log("Error creating start tag : " + err);
                    }
                )
            } else {
                self.log("Invalid switch : " + val);
            }
        }
    };

    type.prototype.clear = function(){
        var console = haku.router.currentView.$el.find('#consoleOut');
        console.html("");
    };

    type.prototype.log = function(message){
        var console = haku.router.currentView.$el.find('#consoleOut');
        console.append("<div>" + message + "</div>");
    };

    type.prototype.dispose = function(){

    };

    klon.register("TagRunner", "AdminContext", type);

}())