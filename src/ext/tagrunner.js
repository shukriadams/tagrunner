require(['haku'], function(){

    TagRunner = {};

    // ===========================================================================
    // Override app
    // ---------------------------------------------------------------------------
    klon.register('haku.application', haku.application.type().extend({

        context : null,

        initialize: function () {
            klon.base(this, "initialize", arguments );

            require.config({
                baseUrl: haku.settings.systemPathRoot,
                paths: {
                    'master': 'ext/views/master',
                    'home': 'ext/views/home',
                    'admin': 'ext/views/admin',
                    'baton': 'ext/views/baton',
                    'batonCore': 'ext/baton',
                    'highScore': 'ext/views/highScore',
                    'success': 'ext/views/success',
                    'controller': 'ext/views/controller',
                    'nfcHelper': 'app/helpers/nfc',
                    'tagHelper': 'ext/tagHelper',
                    'nfcShim': 'shims/com.chariotsolutions.nfc.plugin',
                    'accelerometer' : 'shims/org.apache.cordova.device-motion',
                    'adminContext': 'ext/adminContext',
                    'gameContext': 'ext/gameContext'
                },
                shim: {
                    'nfcHelper': { deps : ['nfcShim'] }
                }
            });

        },

        start : function(){
            klon.base(this, "start", arguments );
            var self = this;


            require([ 'nfcHelper', 'tagHelper', 'accelerometer', 'adminContext', 'gameContext', 'batonCore' ], function(){

                self.context = new TagRunner.GameContext();


                // bind tag scan
                haku.helpers.nfc.bind(function(ndef){
                    var items = TagRunner.TagHelper.readItems(ndef.tag);

                    for (var i = 0 ; i < items.length ; i ++){
                        var item = items[i];

                        if (item.type === "startTag"){
                            $(self).trigger('startTag', ndef);
                            break;
                        } else  if (item.type === "endTag"){
                            $(self).trigger('endTag', ndef);
                            break;
                        }
                    }

                    $(self).trigger('tag', ndef);

                });


                // bind accelerator to app instance and route events through this
                // instance. Done because we cannot easily bind and unbind events
                // to navigator.accelerometer directly, so we need a constant, always-
                // on binder which we can decouple from
                navigator.accelerometer.watchAcceleration(
                    function(acc){

                        $(haku.app).trigger('acceleration', acc);

                    },
                    function(err){
                        // todo handle error
                    },
                    { frequency : 500 }
                );

            });

            function adminContext(){
                if (self.context)
                    self.context.dispose();
                self.context = null;
                self.context = new TagRunner.AdminContext();
                haku.router.navigate("admin", { trigger: true });
            }

            document.addEventListener("menubutton", function(){
                adminContext();
            }, false);

            $(this).on("gameContext", function(){
                if (self.context)
                    self.context.dispose();
                self.context = null;

                self.context = new TagRunner.GameContext();
                haku.router.navigate("home", { trigger: true });
            });



        }

    }));


    // ===========================================================================
    // Override router
    // ---------------------------------------------------------------------------
    klon.register('haku.routers', haku.routers.type().extend({

        initialize : function(){
            klon.base(this, "initialize", arguments);
            var self = this;

            // override underscore default template embed tags
            _.templateSettings = {
                evaluate: /\{%([\s\S]+?)%\}/g,
                interpolate: /\{\{([\s\S]+?)\}\}/g
            };



            require(['master', 'controller', 'nfcShim'], function(){

                // render base body content
                var master = haku.views.master.instance();
                master.render();

                $('[data-layout-role="root"]').append(master.$el);

                // need to get attachment root from body once its rendered
                self.root = $('[data-layout-role="body"]');

                if (haku.settings.platform === "browser"){
                    var controller = haku.views.controller.instance();
                    controller.render();
                    $('[data-layout-role="controller"]').append(controller.$el);
                }

                self.home();
            });

        },

        routes: {
            "home": "home",
            "admin": "admin",
            "baton" : "baton",
            "fail" : "fail",
            "success" : "success",
            "highScore" : "highScore"
        },

        home : function(){
            var self = this;
            require(['home'], function(){
                var view = haku.views.home.instance();

                self.__showPageView(view);
            });
        },

        admin : function(){
            var self = this;
            require(['admin'], function(){
                var view = haku.views.admin.instance();

                self.__showPageView(view);
            });
        },

        baton : function(){
            var self = this;
            require(['baton'], function(){
                var view = haku.views.baton.instance();

                self.__showPageView(view);
            });
        },

        success : function(){
            var self = this;
            require(['success'], function(){
                var view = haku.views.success.instance();

                self.__showPageView(view);
            });
        },

        highScore : function(){
            var self = this;
            require(['highScore'], function(){
                var view = haku.views.highScore.instance();

                self.__showPageView(view);
            });
        },

        __showPageView: function (view) {

            // removes old page out, if page supports t
            var previousView = this.currentView || null;
            if (previousView) {
                previousView.remove();
            }

            view.render();
            //view.$el.addClass('page');

            this.currentView = view;

            //this.root.empty();
            this.root.append(view.$el);

            // call method that tells view its content is now visible
            if (view.__proto__.hasOwnProperty("onShow")){
                view.onShow();
            }
        }

    }));


    // ===========================================================================
    // Start app
    // ---------------------------------------------------------------------------
    document.addEventListener('deviceready', function(){
        var app = haku.application.instance();
        app.start();
    }, false);

    if (haku.settings.launchMode === "direct"){
        document.dispatchEvent(new CustomEvent("deviceready"));
    }

});