(function(){

    // ===========================================================================
    // Game context is the logic context for game play. This handles all events
    // for starting and ending a game, calculating score etc.
    //
    // A game starts when the startTag is scanned. A baton is created, which starts
    // to decay. If the phone is tilted, the baton loses HP. The player wins the
    // game if an endTag is scanned while the baton still has time and HP on it.
    //
    // Player score is calculated based on how much time and HP is left on the
    // baton. The end tag holds the best scores ; if a player beats one of those
    // scores, she can add her score to the list.
    // ---------------------------------------------------------------------------
    var type = function(){
        // current baton object in game
        this.baton = null;

        // score if player wins a round
        this.score = 0;

        // name of player ; entered at game start
        this.playerName = "Player1";

        //
        this.sensitivity = 1;

        // maximum number of items to place in highscore list on end tag
        this.highScoreLength = 2;

        // Holds complex objects from taglist (converted from json to objects).
        // This data is either read directly from the tag, or calculated after
        // player's new score is added to it.
        this.displayHighScoreList = [];

        // if player's score is better than any of the highscore items on endtag,
        // a new highscore will be stored here, which can be written to tag.
        // Else, is null. todo : deprecate
        this.newScore = null;

        var self = this;

        // event handlers
        $(haku.app).on("startTag", function(e, ndef){
            if (haku.app.context != self)
                return;

            self.onStartTag(e, ndef);
        });

        $(haku.app).on("endTag", function(e, ndef){
            if (haku.app.context != self)
                return;

            self.onEndTag(e, ndef);
        });
    };

    type.prototype = function () { this.apply(this, arguments); };


    // ===========================================================================
    //
    // ---------------------------------------------------------------------------
    type.prototype.onAcceleration = function(sender, acc){
        var self = this.context;

        // zero the device with screen facing up. the z axis value at this orientation is -10, so we
        // need to add 10 to compensate, but only when running on an actual device
        var zMod = navigator.accelerometer.shim ? 0 : 10;

        var delta = Math.abs(acc.x) + Math.abs(acc.y) + Math.abs(zMod - acc.z);
        delta = delta * self.sensitivity;
        delta = Math.round(delta);
        self.baton.hp += delta;
    };


    // ===========================================================================
    // Invoked when a start tag is scanned. Creates a baton and displays it.
    // If a baton already exists, it is overwritten.
    // ---------------------------------------------------------------------------
    type.prototype.onStartTag = function(e, ndef){
        var self = this;

        if (self.baton != null){
            self.baton.dispose();
        }

        // try to get name from view if view is home
        if (haku.router.currentView.getName){
            var name = haku.router.currentView.getName();
            if (name.length > 0){
                self.playerName = name;
            }
        }

        // reset game state
        self.score = 0;
        self.newScore = null;

        // try to get start data args from tag
        var items = TagRunner.TagHelper.readItems(ndef.tag);
        for (var i = 0 ; i< items.length ; i ++){
            var item = items[i];
            if (item.type !== "startTag")
                continue;

            if (item.sensitivity)
                self.sensitivity = item.sensitivity
        }

        self.baton = new TagRunner.Baton();

        $(haku.app).bind('acceleration', self.onAcceleration);


        if (navigator.accelerometer.shim){
            navigator.accelerometer.shim.start();
        }

        haku.router.navigate("baton", { trigger: true });
    };


    // ===========================================================================
    // Invoked when end tag is scanned. Three outcomes are possible :
    // 1) The user has a new high score waiting to be written to tag
    // 2) The user is not in a game and merely wants to see high scores on tag
    // 3) The user has an active baton and has just reached the end tag - calculate
    //    score, check if high score on tag has been bested, and if so, queue a
    //    new high score which can be written if the player swipes tag again.
    // ---------------------------------------------------------------------------
    type.prototype.onEndTag = function(e, ndef){
        var self = this;

        // has new score list waiting to write to tag
        if (self.newScore != null){
            haku.helpers.nfc.writeItems(self.newScore,
                function(){
                    self.newScore = null;

                    $(haku.app).trigger("endTagWrite");

                    haku.router.navigate("highScore", { trigger: true })
                },
                function(err){
                    alert('error writing high score  ' + err);
                }
            );

            return;
        }

        // get everything on tag
        var allTagItems = haku.helpers.nfc.readItems(ndef.tag);
        // get only scores from tag, convert to complex objects
        var highScores = [];
        for (var i = 0 ; i < allTagItems.length ; i ++){
            if (allTagItems[i].type === "json/highScore"){
                var highScore = jQuery.parseJSON(allTagItems[i].content);
                highScores.push(highScore);
            }
        }

        // if no baton is active, show high score
        if (self.baton == null){
            // store data for display on high score list
            self.displayHighScoreList = highScores;

            haku.router.navigate("highScore", { trigger: true })
            return;
        }

        // ------------------------------------------------
        // todo : calculate score here.
        self.score = self.baton.hp * self.baton.time;

        var myScore = { score :self.score, name : self.playerName  };
        var added = false;
        var scoreChanged = false;

        // check if player score is lower than anything on high score and if so, insert into score list
        for (var i = 0 ; i < highScores.length ; i ++){
            var highScore = highScores[i];

            // beat existing score, push into score list
            if (self.score < highScore.score){
                highScores.splice(i, 0, myScore);
                added = true;
                scoreChanged = true;
                break;
            }
        }


        // add to end of score list of there is space
        if (!added && highScores.length < self.highScoreLength){
            highScores.splice(highScores.length, 0, myScore);
            scoreChanged = true;
        }


        if (scoreChanged){
            var newScore = [];

            // trim size of score list if needed
            if (highScores.length > self.highScoreLength){
                var newHighScores = [];
                for (var i = 0 ; i < self.highScoreLength ; i ++){
                    newHighScores.push(highScores[i]);
                }
                highScores = newHighScores;
            }

            // store data for display on high score list
            self.displayHighScoreList = highScores;

            // add all non-score items
            for (var i = 0 ; i < allTagItems.length ; i ++){
                if (allTagItems[i].type === "json/highScore")
                    continue;
                newScore.push(allTagItems[i]);
            }

            // add score items
            for (var i = 0 ; i < highScores.length ; i ++){
                var obj = {
                    type : "json/highScore",
                    content : JSON.stringify(highScores[i])
                };
                newScore.push(obj);
            }

            self.newScore = newScore;
        }


        $(haku.app).unbind('acceleration', self.onAcceleration);
        self.baton.dispose();
        self.baton = null;
        haku.router.navigate("success", { trigger: true });
    };


    // ===========================================================================
    // Cleans up everything in context. Called when switching to another context.
    // ---------------------------------------------------------------------------
    type.prototype.dispose = function(){

        if (this.baton != null){
            this.baton.dispose();
        }

        if (navigator.accelerometer.shim){
            navigator.accelerometer.shim.stop();
        }

        $(haku.app).unbind('acceleration');

        $(haku.app).unbind("startTag");
        $(haku.app).unbind("endTag");
    };


    // ===========================================================================
    // Resets all state in this context.
    // ---------------------------------------------------------------------------
    type.prototype.reset = function(){
        if (this.baton != null){
            this.baton.dispose();
        }

        this.score = 0;
        this.newScore = null;

        haku.router.navigate("home", { trigger: true });
    };

    klon.register("TagRunner", "GameContext", type);

}())